<?php
    require_once('sheep.php');
    require_once('frog.php');
    require_once('ape.php');
    $sheep = new Animal("shaun");
    echo "Name : " . $sheep->nama . "<br>"; // "shaun"
    echo "Legs : " . $sheep->legs . "<br>"; // 4
    echo "Cold blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"

    $frog = new frog("Buduk");
    echo "Name : " . $frog->nama . "<br>"; // "shaun"
    echo "Legs : " . $frog->legs . "<br>"; // 4
    echo "Cold blooded : " . $frog->cold_blooded . "<br>"; // "no"
    echo $frog->jump("hop hop") . "<br><br>";

    $ape = new ape("kera sakti");
    echo "Name : " . $ape->nama . "<br>"; // "shaun"
    echo "Legs : " . $ape->legs . "<br>"; // 4
    echo "Cold blooded : " . $ape->cold_blooded . "<br>"; // "no"
    echo $ape->yell("Auooo") . "<br>";




?>